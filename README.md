# sxhkdrc generator
A script written in D which will generate a sxhkdrc file for you!

## Requirements
You need to have `rdmd` installed and in your `$PATH`. `rdmd` is included with `dmd` (found [here](https://dlang.org/download.html)).

## Usage
Using this script is incredibly easy. The script is split up in to three sections (which can be disabled by setting their respective booleans to `false`) to keep things nice and tidy and `sxhkdConfigFile` is the absolute path to your `sxhkdrc`.

An example is included in the script, but I'll reiterate here:
To add a new hotkey add a new `sxhkdEntry` to one of the arrays after the comment explaining how to use the script.

In this example, I'll add two bspwm hotkeys:
```d
immutable sxhkdEntry[] bspwmShortcuts = [
    sxhkdEntry("super + {_,shift} + q", "bspc node -{c,q}", "# Quit or kill a window" ),
    sxhkdEntry("alt + q", "{pkill panel; pkill cat; pkill conky; bspc config top_padding 0 ,\\
    bspc config top_padding 14; $HOME/.config/bspwm/panel/panel & }", "# Toggle panels")
];
```
In the above example, we have a command that only takes up one line and another that takes up multiple lines. To maintain proper formatting in the resulting sxhkdrc, you must include a tab after the newline.
Alternatively, you can write the second entry as: 
```d
sxhkdEntry("alt + q", "{pkill panel; pkill cat; pkill conky; bspc config top_padding 0 ,\\\n\tbspc config top_padding 14; $HOME/.config/bspwm/panel/panel & }", "# Toggle panels")
```
Note the `\n\t`; it inserts a newline and a tab character. Again, this is to preserve the formatting of the sxhkdrc file.

The above example will produce a sxhkdrc file that looks as follows in `/home/[username]/.config/sxhkd/`:
```
# vim: set ft=sxhkdrc:

# SXHKD CONFIG BEGIN


# SECTION: bspwm

# Quit or kill a window
super + {_,shift} + q
	bspc node -{c,q}

# Toggle panels
alt + q
	{pkill panel; pkill cat; pkill conky; bspc config top_padding 0 ,\
	bspc config top_padding 14; $HOME/.config/bspwm/panel/panel & }


# SXHKD CONFIG END
```
After you add all the entries you'd like, simply make sure `sxhkdrcGenerator.d` is executable (`chmod +x sxhkdrcGenerator.d`) and run it like any other shell script!

And with that, happy config-ing!
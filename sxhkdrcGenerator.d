#!/usr/bin/rdmd --shebang -O

/* =============================================================== *
 * Simply set any of these to "false" if you'd like to disable     *
 * that section in the resulting sxhkdrc file                      *
 * =============================================================== */
bool include_section_bspwm      = true; // @suppress(dscanner.style.undocumented_declaration)
bool include_section_keyboard   = false; // @suppress(dscanner.style.undocumented_declaration)
bool include_section_mouse      = false; // @suppress(dscanner.style.undocumented_declaration)

/** Where the sxhkd config file resides
  */
string sxhkdConfigFile = "/home/[username]/.config/sxhkd/sxhkdrc";

/* =============================================================== */

/* How to use:
 * Simply modify the arrays below with whatever you'd like to include
 *  in your sxhkdrc file. For example:
 *
 *  sxhkdEntry("super + {_,shift} + q", "bspc node {-c,-q}", "# Quit or kill")
 *
 * If you would like to split up a command in to multiple lines (for readability), 
 *  simply insert a newline and make sure there is a TAB character before the next
 *  line, like so:
 *
 *  sxhkdEntry("alt + q", "{pkill panel; pkill cat; pkill conky; bspc config top_padding 0 ,\
 *  bspc config top_padding 14; $home/.config/bspwm/panel/panel & }", "# Toggle panels")
 *
 *  Notice how the beginning of the next line lines up with "sxhkdEntry"
 *
 * Alternatively, you can write the command in one continuous line but get the same results
 *  by using control characters:
 *
 *  ... bspc config top_padding 0,\\\n\tbspc config top_padding 14 ...
 *
 *  \n inserts a newline and \t inserts a TAB character. Easy peasy!
 */

/** As sxhkd is commonly used in conjuction with bspwm, I've included
  *  a section specifically for it */
immutable sxhkdEntry[] bspwmShortcuts = [
	sxhkdEntry("super + {_,shift} + q", "bspc node -{c,q}", "# Quit or kill a window" ),
    sxhkdEntry("alt + q", "{pkill panel; pkill cat; pkill conky; bspc config top_padding 0 ,\\
	bspc config top_padding 14; $HOME/.config/bspwm/panel/panel & }", "# Toggle panels")
];

/** Keyboard shortcuts */
immutable sxhkdEntry[] keyboardShortcuts = [
	sxhkdEntry("[Key]", "[Command]", "# [Comment]")
];

/** Mouse shortcuts */
immutable sxhkdEntry[] mouseShortcuts = [
	sxhkdEntry("[Key]", "[Command]", "# [Comment]")
];

/* =============================================================== */

import std.file : write, exists, remove, append;

void main() {

	// This is where the real meat of the program is
	
	// If the config file exists, remove it
	if(sxhkdConfigFile.exists) {
		sxhkdConfigFile.remove;
	}

	// Create a new one and insert the header of the sxhkd file
	sxhkdConfigFile.write("# vim: set ft=sxhkdrc:\n\n# SXHKD CONFIG BEGIN\n\n");

	if(include_section_bspwm) {
		sxhkdConfigFile.append("\n# SECTION: bspwm\n\n");

		foreach(entry; bspwmShortcuts) {
			sxhkdConfigFile.append(entry.toString());
		}
	}

	if(include_section_keyboard) {
		sxhkdConfigFile.append("\n# SECTION: keyboard\n\n");
		foreach(entry; keyboardShortcuts) {
			sxhkdConfigFile.append(entry.toString());
		}
	}

	if(include_section_mouse) {
		sxhkdConfigFile.append("\n# SECTION: mouse\n\n");
		foreach(entry; mouseShortcuts) {
			sxhkdConfigFile.append(entry.toString());
		}
	}

	sxhkdConfigFile.append("\n# SXHKD CONFIG END");
}

/* =============================================================== */

/// An sxhkd entry
struct sxhkdEntry { // @suppress(dscanner.style.phobos_naming_convention)
public:
	/// One input for each line of an sxhkd entry
	this(const string keyCombo, const string action, const string comment) {
		_keyCombo = keyCombo;
		_action = action;
		_comment = comment;
	}

	string toString() @safe const pure nothrow
	{
		return (_comment ~ "\n" 
				 ~ _keyCombo ~ "\n" 
				 ~ "\t" ~ _action ~ "\n\n");
	}

private:
	string _keyCombo, _action, _comment;
}
